﻿using JoelDroidLollipopBatchDeodexer.LBDUtilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace JoelDroidLollipopBatchDeodexer
{
    
    public partial class MainForm : Form
    {
        private string ROM_PATH = string.Empty;
        private DirectoryInfo di_SysApps;
        private DirectoryInfo di_SysPrivApps;
        private DirectoryInfo di_SysFramework;
        private FileInfo fi_SysFrameworkBoot;
        private FileInfo fi_SysFramework64Boot;

        public MainForm()
        {
            InitializeComponent();
            GetVersion();
            CheckJava();
            GlobalVariables.isRom64Bit = false;
        }

        private void button_Browse_RomPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                ROM_PATH = fbd.SelectedPath;
                label_Rom_Path.Text = ROM_PATH;

                
                di_SysApps = new DirectoryInfo(ROM_PATH + @"\app");
                di_SysPrivApps = new DirectoryInfo(ROM_PATH + @"\priv-app");
                di_SysFramework = new DirectoryInfo(ROM_PATH + @"\framework");
                fi_SysFrameworkBoot = new FileInfo(ROM_PATH + @"\framework\arm\boot.oat");
                fi_SysFramework64Boot = new FileInfo(ROM_PATH + @"\framework\arm64\boot.oat");
            }
        }

        

        private void button_Start_DeOdex_Click(object sender, EventArgs e)
        {
            if (RomFolderIsValid())
            {
                EnableControls(false);
                backgroundDeOdexWorker.RunWorkerAsync();
            }
            else
            {
                ShowErrorMessage("Invalid Rom Folder", "Error");
            }
        }

        private void backgroundDeOdexWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DeOdexMain();
        }

        private void backgroundDeOdexWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundDeOdexWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnableControls(true);
            CleanWorkingDirectory(true);
            Logger.WriteLog();
        }

        #region Private Mothods

        #region UI Methods

        private void GetVersion()
        {
            string appVersionText = "JoelDroid Lollipop Batch Deodexer";
            try
            {
                string version = ConfigurationManager.AppSettings["currentVersion"].Trim();
                appVersionText = appVersionText + " Version " + version;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex.Message, ex.StackTrace);
            }

            this.Text = appVersionText;
        }

        private void ShowErrorMessage(string Message, string Title)
        {
            MessageBox.Show(Message, Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void UpdateUILog(string logEntry)
        {
            logListBox.Invoke((Action)(() =>
            {
                logListBox.Items.Add(logEntry);
                logListBox.TopIndex = logListBox.Items.Count - 1;
            }));

            Logger.Log(logEntry);
        }

        private void EnableControls(bool status)
        {
            button_Browse_RomPath.Enabled = status;
            button_Start_DeOdex.Enabled = status;
        }

        #endregion

        #region Utility Methods

        private bool RomFolderIsValid()
        {
            bool isRomFolderValid = false;
            if (!string.IsNullOrEmpty(ROM_PATH))
            {
                DirectoryInfo romFolderPath = new DirectoryInfo(ROM_PATH);
                if (romFolderPath.Exists)
                {
                    if (!ToolPaths.TOOLS_PATH.Contains(" ") && !ROM_PATH.Contains(" "))
                    {
                        if (di_SysFramework.Exists)
                        {
                            UpdateUILog("Framework Folder Exists");
                            if (fi_SysFrameworkBoot.Exists || fi_SysFramework64Boot.Exists)
                            {
                                UpdateUILog("boot.oat Exists");
                                if (fi_SysFramework64Boot.Exists)
                                {
                                    UpdateUILog("64-bit Rom Detected");
                                    GlobalVariables.isRom64Bit = true;
                                }
                                if (di_SysApps.Exists)
                                {
                                    UpdateUILog("App Folder Exists");
                                }
                                if (di_SysPrivApps.Exists)
                                {
                                    UpdateUILog("Priv-App Folder Exists");
                                }
                                isRomFolderValid = true;
                            }
                            else
                            {
                                ShowErrorMessage("Boot.oat not found", "Invalid Directory");
                            }
                        }
                        else
                        {
                            ShowErrorMessage("Framework folder not found", "Invalid Directory");
                        }
                    }
                    else
                    {
                        ShowErrorMessage("Application folder and Rom folder should not have spaces", "Invalid Application Directory");
                    }
                }
            }
            return isRomFolderValid;
        }

        private void CheckJava()
        {
            string javaVersion = JarRunner.JavaPathVariableAndVersionCheck();
            if (!string.IsNullOrEmpty(javaVersion) && javaVersion.Length > 3 && javaVersion.Contains("_"))
            {
                label_Java_CurrentVersion.Text = JavaVersionFormatter(javaVersion);
                EnableControls(true);
            }
            else
            {
                label_Java_CurrentVersion.Text = "Java Not Installed or Path Variable Not Set Up";
                ShowErrorMessage("Install Java, and setup Path variable", "Java Missing");
                EnableControls(false);
            }
        }

        private string JavaVersionFormatter(string javaString)
        {
            string formatted = "Unknown Version";
            if (javaString.Length > 3 && javaString.Contains("_"))
            {
                string[] javaArray = javaString.Split('_');
                string mainVersion = javaArray[0].Substring(2, 1);
                string updateVersion = javaArray[1];

                formatted = string.Format("Java {0} Update {1}", mainVersion, updateVersion);
            }

            return formatted;
        }

        private static void CleanWorkingDirectory(bool isExit)
        {
            DirectoryInfo diWorking = new DirectoryInfo(ToolPaths.WORKING_PATH);
            try
            {
                if (diWorking.Exists)
                {
                    diWorking.Delete(true);
                }

                if (!isExit)
                {
                    diWorking.Create();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex.Message, ex.StackTrace);
            }
        }

        #endregion

        #region Deodex Methods

        private void DeOdexMain()
        {
            UpdateUILog("Deodexing Started");
            CleanWorkingDirectory(false);
            
            if (DecodeBootOat())
            {
                DeodexSystemApps();
                DeOdexPrivateApps();
                DeOdexFrameworkApps();
                DeOdexBootFrameworkApps();

                FileOperations.CleanFrameworkArmFolders(di_SysFramework);

                UpdateUILog(string.Empty);
                UpdateUILog("Deodexing Completed");
            }
            else 
            {
                UpdateUILog(string.Empty);
                UpdateUILog("Deodexing Failed");
            }
        }        

        private bool DecodeBootOat()
        {
            bool isBootOatExtractionComplete = false;
            bool isBootOatCopied = false;
            UpdateUILog("Extracting Boot.oat");
            
            if (GlobalVariables.isRom64Bit)
            {
                isBootOatCopied = FileOperations.CopyBootOat(fi_SysFramework64Boot);
                
            }
            else
            {
                isBootOatCopied = FileOperations.CopyBootOat(fi_SysFrameworkBoot);
            }

            if (isBootOatCopied)
            {
                UpdateUILog("Copying Boot.oat to Working Directory - SUCCESS");
                isBootOatExtractionComplete = JarRunner.DecodeBootOat();
            }

            if (isBootOatExtractionComplete)
            {
                UpdateUILog("Boot.oat Extraction Complete");
            }
            else
            {
                UpdateUILog("Boot.oat Extraction Failed");
            }
            return isBootOatExtractionComplete;
        }

        private void DeodexSystemApps()
        {
            List<DirectoryInfo> SystemApps = FileOperations.GetAllApps(di_SysApps);

            DeodexApps(SystemApps, "System");
        }

        private void DeOdexPrivateApps()
        {
            DirectoryInfo di_SysPrivApps = new DirectoryInfo(ROM_PATH + @"\priv-app");
            List<DirectoryInfo> PrivApps = FileOperations.GetAllApps(di_SysPrivApps);

            DeodexApps(PrivApps, "Private");
        }

        private void DeOdexFrameworkApps()
        {
            DirectoryInfo di_SysFramework = new DirectoryInfo(ROM_PATH + @"\framework");
            List<FileInfo[]> FrameworkApps = FileOperations.GetAllFrameworkApps(di_SysFramework);

            if (FrameworkApps.Count > 0)
            {
                UpdateUILog(string.Empty);
                UpdateUILog(string.Format("Odexed Framework Apps Count: {0}", FrameworkApps.Count));
                UpdateUILog("Deodexing Framework Apps");
                UpdateUILog(string.Empty);

                DeodexFrameworkApp(FrameworkApps);
            }
        }

        private void DeOdexBootFrameworkApps()
        {
            DirectoryInfo di_SysFramework = new DirectoryInfo(ROM_PATH + @"\framework");
            List<FileInfo[]> BootFrameworkApps = FileOperations.GetAllBootFrameworkApps(di_SysFramework);

            if (BootFrameworkApps.Count > 0)
            {
                UpdateUILog(string.Empty);
                UpdateUILog(string.Format("Odexed Boot Framework Apps Count: {0}", BootFrameworkApps.Count));
                UpdateUILog("Deodexing Boot Framework Apps");
                UpdateUILog(string.Empty);

                DeodexBootFrameworkApp(BootFrameworkApps);

                DeodexClasses2();
            }
        }

        private void DeodexClasses2()
        {
            FileInfo fiClassesDex = new FileInfo(ToolPaths.BOOT_OAT_DEX_PATH + @"\framework-classes2.dex");
            FileInfo fiClassesJar = new FileInfo(ROM_PATH + @"\framework\framework.jar");

            if (fiClassesDex.Exists && fiClassesJar.Exists)
            {
                FileInfo[] frameworkApp = new FileInfo[2] { fiClassesJar, fiClassesDex };
                string appName = frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4));
                int errorCode = DeOdexer.DeodexClasses2(frameworkApp);

                if (errorCode == 0)
                {
                    UpdateUILog("Deodexing Classes2 - SUCCESS");
                }
                else
                {
                    string deodexMessage = DeOdexer.GetDeodexMessage(errorCode);
                    UpdateUILog(string.Format("Deodexing Classes2 - FAILED - {0}", deodexMessage));
                }

                FileOperations.CleanWorkingFolderAppDirectory(appName);
            }
        }

        private void DeodexApps(List<DirectoryInfo> AppList, string AppTypeLabel)
        {
            int appCounter = 1;
            string deodexMessage = string.Empty;
            if (AppList.Count > 0)
            {
                UpdateUILog(string.Empty);
                UpdateUILog(string.Format("Odexed {0} Apps Count: {1}", AppTypeLabel, AppList.Count));
                UpdateUILog(string.Format("Deodexing {0} Apps", AppTypeLabel));
                UpdateUILog(string.Empty);

                foreach (DirectoryInfo appDirectory in AppList)
                {
                    int errorCode = DeOdexer.DeodexApp(appDirectory);
                    if (errorCode == 0)
                    {
                        UpdateUILog(string.Format("{0} of {1} - {2} - SUCCESS", appCounter, AppList.Count, appDirectory.Name));
                    }
                    else
                    {
                        deodexMessage = DeOdexer.GetDeodexMessage(errorCode);
                        UpdateUILog(string.Format("{0} of {1} - {2} - FAILED - {3}", appCounter, AppList.Count, appDirectory.Name, deodexMessage));
                    }

                    appCounter++;
                    FileOperations.CleanWorkingFolderAppDirectory(appDirectory.Name);
                }
            }
        }

        private void DeodexFrameworkApp(List<FileInfo[]> FrameworkApps)
        {
            int appCounter = 1;
            string deodexMessage = string.Empty;

            foreach (FileInfo[] frameworkApp in FrameworkApps)
            {
                string appName = frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4));
                int errorCode = DeOdexer.DeodexFrameworkApp(frameworkApp);

                if (errorCode == 0)
                {
                    UpdateUILog(string.Format("{0} of {1} - {2} - SUCCESS", appCounter, FrameworkApps.Count, appName));
                }
                else
                {
                    deodexMessage = DeOdexer.GetDeodexMessage(errorCode);
                    UpdateUILog(string.Format("{0} of {1} - {2} - FAILED - {3}", appCounter, FrameworkApps.Count, appName, deodexMessage));
                }

                appCounter++;
                FileOperations.CleanWorkingFolderAppDirectory(appName);
            }
        }

        private void DeodexBootFrameworkApp(List<FileInfo[]> FrameworkApps)
        {
            int appCounter = 1;

            foreach (FileInfo[] frameworkApp in FrameworkApps)
            {
                string appName = frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4));
                int errorCode = DeOdexer.DeodexBootFrameworkApp(frameworkApp);

                if (errorCode == 0)
                {
                    UpdateUILog(string.Format("{0} of {1} - {2} - SUCCESS", appCounter, FrameworkApps.Count, appName));
                }
                else
                {
                    string deodexMessage = DeOdexer.GetDeodexMessage(errorCode);
                    UpdateUILog(string.Format("{0} of {1} - {2} - FAILED - {3}", appCounter, FrameworkApps.Count, appName, deodexMessage));
                }

                appCounter++;
                FileOperations.CleanWorkingFolderAppDirectory(appName);
            }
        }
        
        #endregion

        #endregion
    }
}
