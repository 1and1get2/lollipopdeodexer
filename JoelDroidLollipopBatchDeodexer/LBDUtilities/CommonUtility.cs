﻿using JoelDroidLollipopBatchDeodexer.LBDObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace JoelDroidLollipopBatchDeodexer.LBDUtilities
{
    public class CommonUtility
    {
        public static ProcessOutput RunProcess(ProcessInput processInput)
        {
            ProcessOutput procecessOut = new ProcessOutput();
            procecessOut.ProcessStatus = false;
            List<string> outputList = new List<string>();
            Process process = new Process();
            try
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.FileName = processInput.FileName;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.WorkingDirectory = processInput.WorkingDirectory;
                process.StartInfo.Arguments = processInput.Arguments;

                process.OutputDataReceived += new DataReceivedEventHandler((s, e) =>
                {
                    if (e.Data != null)
                    {
                        outputList.Add((string)e.Data);
                    }
                });
                process.ErrorDataReceived += new DataReceivedEventHandler((s, e) =>
                {
                    if (e.Data != null)
                    {
                        outputList.Add((String)e.Data);
                    }
                });

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();

                procecessOut.ProcessExitCode = process.ExitCode;
                procecessOut.ProcessOutputStringList = outputList;
                procecessOut.ProcessStatus = true;
            }
            catch (Exception ex)
            {
                procecessOut.ProcessStatus = false;
                Logger.LogException(ex.Message, ex.StackTrace);
            }
            return procecessOut;
        }

        public static FileInfo[] GetFilesByExtensions(DirectoryInfo dir, bool AllDirectories, params string[] extensions)
        {
            List<FileInfo> files = new List<FileInfo>();
            foreach (string ext in extensions)
            {
                if (AllDirectories)
                {
                    files.AddRange(dir.GetFiles(ext, SearchOption.AllDirectories));
                }
                else
                {
                    files.AddRange(dir.GetFiles(ext));
                }

            }
            return files.ToArray();
        }
    }
}
