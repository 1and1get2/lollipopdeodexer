﻿using JoelDroidLollipopBatchDeodexer.LBDObjects;
using System;
using System.IO;

namespace JoelDroidLollipopBatchDeodexer.LBDUtilities
{
    public class JarRunner
    {
        public static string JavaPathVariableAndVersionCheck() 
        {
            ProcessInput processIn = new ProcessInput();
            processIn.FileName = "cmd.exe";
            processIn.Arguments = "/c \"" + "java -version " + "\"";
            processIn.WorkingDirectory = System.Environment.CurrentDirectory;

            ProcessOutput processOut = CommonUtility.RunProcess(processIn);

            if (processOut.ProcessStatus && processOut.ProcessOutputStringList.Count > 0)
            {
                string JavaVersion = processOut.ProcessOutputStringList[0];
                string JavaVersionNumber = JavaVersion.Substring(JavaVersion.IndexOf('"')).Replace("\"", string.Empty);
                String JavaVersionString = JavaVersionNumber;
                return JavaVersionString;
            }
            else
            {
                return null;
            }
        }

        public static bool DecodeBootOat()
        {
            bool isBootOatDecode = false;
            FileInfo fiOat = new FileInfo(ToolPaths.BOOT_OAT_PATH);
            FileInfo fiOat2Dex = new FileInfo(ToolPaths.OAT2DEXJAR_PATH);
            if (fiOat2Dex.Exists && fiOat.Exists)
            {
                string commandString = string.Format("java -jar {0} boot {1}", fiOat2Dex.FullName, fiOat.FullName);
                
                ProcessInput processIn = new ProcessInput();
                processIn.FileName = "cmd.exe";
                processIn.Arguments = "/c \"" + commandString + "\"";
                processIn.WorkingDirectory = System.Environment.CurrentDirectory;

                ProcessOutput processOut = CommonUtility.RunProcess(processIn);
                DirectoryInfo diBoot = new DirectoryInfo(fiOat.Directory.FullName + @"\odex");
                isBootOatDecode = (diBoot.Exists && diBoot.GetFiles("*.dex", SearchOption.TopDirectoryOnly).Length > 0);                
            }

            return isBootOatDecode;
        }

        public static bool Oat2Dex(string appName)
        {
            bool isOat2DexSuccess = false;
            FileInfo fiOat = new FileInfo(string.Format(ToolPaths.UNZIP_OUT_PATH, appName));
            FileInfo fiOat2Dex = new FileInfo(ToolPaths.OAT2DEXJAR_PATH);
            DirectoryInfo diBootOdex = new DirectoryInfo(ToolPaths.BOOT_OAT_ODEX_PATH);
            if (fiOat2Dex.Exists && fiOat.Exists && diBootOdex.Exists)
            {
                string commandString = string.Format("java -jar {0} {1} {2}", fiOat2Dex.FullName, fiOat.FullName, diBootOdex.FullName);

                ProcessInput processIn = new ProcessInput();
                processIn.FileName = "cmd.exe";
                processIn.Arguments = "/c \"" + commandString + "\"";
                processIn.WorkingDirectory = System.Environment.CurrentDirectory;

                ProcessOutput processOut = CommonUtility.RunProcess(processIn);

                FileInfo tempDexOut = new FileInfo(string.Format(ToolPaths.TEMP_DEX_PATH, appName));
                isOat2DexSuccess = tempDexOut.Exists;
            }

            return isOat2DexSuccess;
        }
    }
}
