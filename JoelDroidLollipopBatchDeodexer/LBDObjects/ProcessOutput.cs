﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoelDroidLollipopBatchDeodexer.LBDObjects
{
    public class ProcessOutput
    {
        public bool ProcessStatus { get; set; }
        public int ProcessExitCode { get; set; }
        public List<string> ProcessOutputStringList { get; set; }
    }
}
